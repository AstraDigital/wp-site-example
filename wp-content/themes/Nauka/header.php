<!DOCTYPE html>
<html lang="ru">

<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <!-- SEO Meta -->
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-touch-icon-114x114.png">
    <!-- Title -->
    <title><?php the_title(); ?></title>

    <?php wp_head(); ?>




</head>  <body>


  <!-- Menu -->
<div class="menu-dark-bg"></div>
<div id="menu">
    <div class="menu-inner">
        <div class="main-menu-toggle">
            <div class="one"></div>
            <div class="two"></div>
            <div class="three"></div>
        </div>

        	<?php 
        	wp_nav_menu();
        	?>           
    </div>
</div>
<!-- Navbar -->
<nav id="navbar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="left-side">
                    <div class="logo">
                            <?php the_custom_logo(); ?>
                            </div>
                </div>
                <div class="right-side">
                    <a href="<?php echo get_field('номер_телефона', 'option'); ?>" class="phone"><img src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.png" alt="alt"><?php echo get_field('номер_телефона', 'option'); ?></a>
                    <?= do_shortcode('[button_register_login]'); ?>
                    

                    <div class="menu-button">
                        <div class="menu-title">Меню</div>
                        <div class="main-menu-toggle-white">
                            <div class="one"></div>
                            <div class="two"></div>
                            <div class="three"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- Header -->
<header id="header">
    <div class="header-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="header-descr">
                        <div class="header-text">
                            <?php if(get_field('первый_заголовок') != ''){
                                echo '<h1>' . get_field('первый_заголовок') . '</h1>';                                
                            }else {
                                echo '<h1>Помогаем студентам быть отличниками.</h1>';
                            }
                            if(get_field('второй_заголовок') != ''){
                                echo '<h2>' . get_field('второй_заголовок') . '</h2>';
                            }else {
                                echo ' <h2>Обращаясь к нам – вы гарантированно всё сдадите!</h2>';
                            }if(get_field('текст') != ''){
                                echo '<p>' . get_field('текст') . '</p>';
                            }else {
                                echo '<p>Напишем для вас все виды работ, от реферата до диплома</p>';
                            } ?>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-9 inline-register"><?= do_shortcode('[button_register_line]'); ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="header-img">
                    <?php $image = get_field('картинка_в_шапке', 'option'); ?><img src="<?php echo $image['url']; ?>" alt="alt"></div>
                </div>
            </div>
        </div>
    </div>
</header>