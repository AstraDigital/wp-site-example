<?php 

// подключаем стили и скрипты
function register_styles_scripts() {
  //стили
  wp_register_style('bootstrap', get_template_directory_uri() .
    '/assets/css/bootstrap.min.css');
  wp_enqueue_style('bootstrap');

  wp_register_style('magnificpopup', get_template_directory_uri() .
    '/assets/css/magnific-popup.css');
  wp_enqueue_style('magnificpopup');

  wp_register_style('owlcarousel', get_template_directory_uri() .
    '/assets/css/owl.carousel.min.css');
  wp_enqueue_style('owlcarousel');

  wp_register_style('owlthemedefault', get_template_directory_uri() .
    '/assets/css/owl.theme.default.min.css');
  wp_enqueue_style('owlthemedefault');

  wp_register_style('select2', get_template_directory_uri() .
    '/assets/css/select2.min.css');
  wp_enqueue_style('select2');

  wp_register_style('bootstrapslider', get_template_directory_uri() .
    '/assets/css/bootstrap-slider.min.css');
  wp_enqueue_style('bootstrapslider');

  wp_register_style('bootstrapdatepicker', get_template_directory_uri() .
    '/assets/css/bootstrap-datepicker.min.css');
  wp_enqueue_style('bootstrapdatepicker');

  wp_register_style('main', get_template_directory_uri() .
    '/assets/css/main.css');
  wp_enqueue_style('main');
  


}

add_action('wp_enqueue_scripts', 'register_styles_scripts');



add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
function my_scripts_method() {
  wp_deregister_script( 'jquery' );
  wp_register_script( 'jquery', 'https://code.jquery.com/jquery-3.2.1.min.js');
  wp_enqueue_script( 'jquery' );

  wp_enqueue_script( 'bootstrap.js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery') );
  wp_enqueue_script( 'SmoothScroll', get_template_directory_uri() . '/assets/js/SmoothScroll.js', array('jquery') );
  wp_enqueue_script( 'matchHeight', get_template_directory_uri() . '/assets/js/jquery.matchHeight-min.js', array('jquery') );
  wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.js', array('jquery') );
  wp_enqueue_script( 'owl.carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery') );
  wp_enqueue_script( 'select2.full.min.js', get_template_directory_uri() . '/assets/js/select2.full.min.js', array('jquery') );
  wp_enqueue_script( 'bootstrap-slider.min.js', get_template_directory_uri() . '/assets/js/bootstrap-slider.min.js', array('jquery') );
  wp_enqueue_script( 'bootstrap-datepicker.min.js', get_template_directory_uri() . '/assets/js/bootstrap-datepicker.min.js', array('jquery') );
  wp_enqueue_script( 'bootstrap-datepicker.ru.min.js', get_template_directory_uri() . '/assets/js/bootstrap-datepicker.ru.min.js', array('jquery') );
  wp_enqueue_script( 'jquery.metisMenu.js', get_template_directory_uri() . '/assets/js/jquery.metisMenu.js', array('jquery') );
  wp_enqueue_script( 'common.js', get_template_directory_uri() . '/assets/js/common.js', array('jquery') );
  wp_enqueue_script( 'jquery.metisMenu.js', get_template_directory_uri() . '/assets/js/jquery.metisMenu.js', array('jquery') );
}




add_theme_support( 'custom-logo' );

add_action( 'after_setup_theme', 'theme_register_nav_menu' );
function theme_register_nav_menu() {
  register_nav_menu( 'primary', 'Primary Menu' );
}

add_theme_support( 'custom-logo', [
  'height'      => 300,
  'width'       => 300,
  'flex-width'  => false,
  'flex-height' => false,
  'header-text' => '',
] );


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Основные настройки',
    'menu_title'  => 'Настройки темы',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
};








 ?>