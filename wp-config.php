<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'wp_dump' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'dbUser' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'dbPassword' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '|cTs_YA^d*FBYYp`nWr3X*pl0?h~/.&t9C8#gpTv@X{^Qi^:6-KjV3tXi0xI8/4R' );
define( 'SECURE_AUTH_KEY',  'ItVksNQBILV6S%G|Y{FbySZP1VPqpNuA@w-(5Xr=f:!,yT.T ]F}Kiv|3@x2,TzD' );
define( 'LOGGED_IN_KEY',    'K4gsw=FpbWnlQ*lnN2g!/m.Y`M#&>MMdnHICeFuKv1~k~Gl F(ZUWX_U)z93@X`%' );
define( 'NONCE_KEY',        '[zGPdn^@!y~|~F@#Sj!BJ ,#=k[h}lRgAI*+Mikw|51<;CD7`0Z4xkwNkMV5aa>V' );
define( 'AUTH_SALT',        '9/85nZ[j~ (z]4V[IT&at49]tsBfU8A,-D7@?w,8cWmXf4t^IbmHa$om?Klrdw-?' );
define( 'SECURE_AUTH_SALT', '@F.Q$L=1[UqK$C}[4H?6o?k#z]pX}+EU>FAwp]S7%N:TL9R?D{F,ls;D0.nR{l6 ' );
define( 'LOGGED_IN_SALT',   'p66oPYW&@5AG?c5J::.qnU*nK=JzJ9X~}h3:%mwlL{}ghC^}~6]w<[(-f#.ecVz;' );
define( 'NONCE_SALT',       'NlW.I}(eoIi94R/)6VDWw8tB4.Vr7!muWmhd>9JA>BW;ILaFV(N!8/&epT3E#Xmn' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
