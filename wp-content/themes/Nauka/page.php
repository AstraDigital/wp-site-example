<?php get_header(); ?>

	

<section id="faq">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2><?php the_title(); ?></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 faq-row">
                    <?php the_post(); ?>
                    <?php the_content(); ?>
                     


                </div>
            </div>

        </div>

    </section>

<?php get_footer(); ?>
