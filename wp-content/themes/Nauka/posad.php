<?php
/*
Template Name: Посадочная
Template Post Type: post, page, product
*/
?>

<?php get_header(); ?>  

<?php if( get_field('Показать', 'option') ): ?>
  
  <!-- Advantages -->
    <section id="advantages">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
            <div class="section-title">
              <h2><?php echo get_field('заголовок_почему', 'option'); ?></h2>
            </div>
            <div class="row">
               <?php
          if( have_rows('преимущества', 'option') ):

         while ( have_rows('преимущества', 'option') ) : the_row();

              ?>
                 <?php echo '<div class="col-md-4 col-sm-6">'; ?>
                 <?php echo '<div class="advantages-item">'; ?>
                 <?php echo '<div class="icon">'; ?>

                <?php $image = get_sub_field('преимущества_картинка', 'option'); ?>
                 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                 <?php echo '</div>'; ?>
                 
                 <?php echo '<h3>' . get_sub_field('преимущества_заголовок', 'option') .'</h3>'; ?>

                 <?php echo '<p>' . get_sub_field('преимущества_текст', 'option') .'</p></div></div>'; ?>

              <?php

            endwhile;

            else :

            endif;
            ?>
            </div>
          </div>
<!--          ==========================================================================================================-->
          <div class="col-md-5">
            
            <div class="section-title">
              <h2>Подсчитайте примерную стоимость работы</h2>
            </div>
              <?= do_shortcode('[na5ku_inline_calculator]'); ?>
          </div>
<!--          ==========================================================================================================-->


        </div>
      </div>
    </section>

    <?php endif; ?>
    <!-- Work -->

    <?php if( get_field('Показать_как_это_работает', 'option') ): ?>
    <section id="work">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-title">
              <h2><?php echo get_field('как_это_работает_заголовок', 'option'); ?></h2>
            </div>
          </div>
        </div>
        <div class="row work-row">
          <div class="col-md-12">
             <?php
          if( have_rows('как_это_работает', 'option') ):

         while ( have_rows('как_это_работает', 'option') ) : the_row();
                 $i++;
              ?> 
                 <?php echo '<div class="work-item">'; ?>
                 <?php echo '<div class="icon">'; ?>

                <?php $image = get_sub_field('картинка_как_это_работает', 'option'); ?>
                 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                 <?php echo '</div>'; ?>

                 <?php echo '<div class="number"><span>' . $i . '</span></div>'; ?>               

                 <?php echo '<p>' . get_sub_field('текст_как_это_работает', 'option') .'</p></div>'; ?>

              <?php

            endwhile;

            else :

            endif;
            ?>
          </div>
        </div>
      </div>
    </section>
    <?php endif; ?>
    <!-- Reviews -->
     <?php if( get_field('Показать_отзывы', 'option') ): ?>
    <section id="reviews">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul class="nav nav-tabs reviews-tabs">
              <li class="active"><a style="height: 46px" data-toggle="tab" href="index.html#tab1">Последние выполненые работы</a></li>
              <li><a data-toggle="tab" href="index.html#tab2"><?php echo get_field('отзывы_заголовок', 'option'); ?></a></li>
            </ul>
            <div class="tab-content">
              <div id="tab1" class="tab-pane fade in active">
                <div class="table-responsive">
                                    <table class="table">
                    <thead>
                      <tr>
                        <th>Дата</th>
                        <th>Номер</th>
                        <th>Тема</th>
                        <th>Тип</th>
                      </tr>
                    </thead>
<?php 
$table = get_field( 'последние_выполненые_работы' , 'option' );

if ( ! empty ( $table ) ) {

    // echo '<table border="0">';

        if ( ! empty( $table['caption'] ) ) {

            echo '<caption>' . $table['caption'] . '</caption>';
        }

        if ( ! empty( $table['header'] ) ) {

            echo '<thead>';

                echo '<tr>';

                    foreach ( $table['header'] as $th ) {

                        echo '<th>';
                            echo $th['c'];
                        echo '</th>';
                    }

                echo '</tr>';

            echo '</thead>';
        }

        echo '<tbody id="tbody">';

            foreach ( $table['body'] as $tr ) {
                echo '<tr>';
                        echo '<td class="time"></td>';
                        echo '<td class="id"></td>';
                    foreach ( $tr as $td ) {
                        
                        echo '<td>';
                            echo $td['c'];
                        echo '</td>';
                    }

                echo '</tr>';
            }

        echo '</tbody>';

    // echo '</table>';
} ?>
                  </table>
                </div>
              </div>
              <div id="tab2" class="tab-pane fade">
                <div class="reviews-carousel owl-carousel owl-theme">
                  <?php
          if( have_rows('отзывы', 'option') ):

         while ( have_rows('отзывы', 'option') ) : the_row();
                 $i++;
              ?> 
                 <?php echo '<div class="item">';
                       echo '<div class="reviews-top clearfix">';
                       echo '<div class="left-side">';
                       echo '<div class="reviews-login">'. get_sub_field('отзывы_имя', 'option') .'</div>';
                       echo '<div class="reviews-date">'. get_sub_field('дата_отзыва', 'option') .'</div>';
                       echo '</div>';
                       echo '<div class="reviews-rating"><img src="' . get_template_directory_uri() . '/img/stars.png" alt="alt"></div>';
                       echo '</div>';
                       echo '<div class="reviews-body">';
                       echo '<p>'. get_sub_field('текст_отзыва', 'option') .'</p>';
                       echo '</div>';
                       echo '</div>';
                        ?>

              <?php

            endwhile;

            else :

            endif;
            ?> 

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
     <?php endif; ?>

     <!-- Stats -->
     <?php if( get_field('показать_статистику', 'option') ): ?>
    <section id="stats">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4">
            <div class="stats-item">
              <div class="counter"><?php echo get_field('статистика_число_1', 'option'); ?></div>
              <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/img/s-icon-01.png" alt="alt"></div>
              <h4><?php echo get_field('статистика_заголовок_1', 'option'); ?></h4>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="stats-item">
              <div class="counter"><?php echo get_field('статистика_число_2', 'option'); ?></div>
              <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/img/s-icon-02.png" alt="alt"></div>
              <h4><?php echo get_field('статистика_заголовок_2', 'option'); ?></h4>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="stats-item">
              <div class="counter"><?php echo get_field('статистика_число_3', 'option'); ?></div>
              <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/img/s-icon-03.png" alt="alt"></div>
              <h4><?php echo get_field('статистика_заголовок_3', 'option'); ?></h4>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php endif; ?>
    
    <?php if( get_field('Показать_безопасность', 'option') ): ?>
    <!-- Safe -->
    <section id="safe">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-4">
            <div class="safe-img"><img src="<?php echo get_template_directory_uri(); ?>/img/safe.png" alt="alt"></div>
          </div>
          <div class="col-md-9 col-sm-8">
            <div class="safe-descr">
              <h3><?php echo get_field('безопасность_заголовок', 'option'); ?></h3>
              <p><?php echo get_field('безопасность_текст', 'option'); ?></p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php endif; ?>
    <!-- Universities -->
    <?php if( get_field('университеты_показать', 'option') ): ?>
    <section id="universities">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-title">
              <h2><?php echo get_field('университеты_заголовок', 'option'); ?></h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="logo-wrap">
                 <?php
          if( have_rows('логотипы_университетов', 'option') ):

         while ( have_rows('логотипы_университетов', 'option') ) : the_row();
                 $i++;
              ?> 
                 <?php echo '<div class="logo-item">';
                       $image = get_sub_field('логотип', 'option'); ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                       <?php echo '</div>'; ?>
              <?php

            endwhile;

            else :

            endif;
            ?> 
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php endif; ?>
    <?php if( get_field('Оформите_заказ_показать', 'option') ): ?>
    <!-- Order -->
    <section id="order">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="form-title">
              <h2><?php echo get_field('Оформите_заказ_заголовок', 'option'); ?></h2>
              <p><?php echo get_field('Оформите_заказ_текст', 'option'); ?></p>
            </div>
            <form class="contacts-form" id="reg-form1">
              <div class="row">
                <div class="col-md-3 col-sm-3 form-group">
                  <!-- список типов работ -->
                    <select name="select" required class="form-control select-input">
                    <option value=1>Бизнес-план</option><option value=2>Дипломная работа (Бакалавр, Специалист)</option><option value=3>Диссертация</option><option value=4>Задача</option><option value=5>Контрольная работа</option><option selected value=6>Курсовая работа</option><option value=7>Лабораторная работа</option><option value=8>Ответы на билеты</option><option value=9>Отчет по практике</option><option value=10>Перевод</option><option value=11>Презентация PowerPoint</option><option value=12>Реферат</option><option value=13>Сочинение</option><option value=14>Тест</option><option value=15>Чертеж</option><option value=16>Эссе</option><option value=17>Решение экзаменов онлайн</option><option value=18>Статья</option><option value=19>Дипломная работа (Магистр)</option><option value=20>План</option>
                  </select>
                </div>

                <div class="col-md-3 col-sm-3 form-group">
                  <input type="email" name="mail" placeholder="Почта" required class="form-control">
                </div>
                <div class="col-md-3 col-sm-3 form-group">
                  <button class="button" onclick="new_register1(); return false;">Заказать</button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-4">
            <div class="order-img"><img src="<?php echo get_template_directory_uri(); ?>/img/order-img.png" alt="alt"></div>
          </div>
        </div>
      </div>
    </section>
     <?php endif; ?>
    <!-- FAQ -->
    <?php if( get_field('Показать_ответы', 'option') ): ?>
    <section id="faq">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title">
          <h2><?php echo get_field('ответы_на_вопросы_заголовок', 'option'); ?></h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2 faq-row">
         <?php
          if( have_rows('ответы_на_вопросы', 'option') ):

         while ( have_rows('ответы_на_вопросы', 'option') ) : the_row();
              ?> 
                 <?php echo '<div class="faq-item" style="display: block;">';
                       echo '<h4>' . get_sub_field('вопрос', 'option') . '</h4>';
                       echo '<p>' . get_sub_field('ответ', 'option') . '</p>';
                       echo '</div>'; ?>
              <?php

            endwhile;

            else :

            endif;
            ?> 

      </div>
    </div>
<!--    <div class="row">-->
<!--      <div class="col-md-12">-->
<!--        <button id="load-more-button" class="button more-button">Читать все</button>-->
<!--      </div>-->
<!--    </div>-->
  </div>

</section>
<?php endif; ?>
    <!--    --->
    <section id="stats">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8 col-md-offset-2 faq-row" id="all_stats">
                        <?php 
                        the_post();
                        the_content(); ?>
                        <style>
                            #all_stats li:before {
                                content: "\2666";
                                color: #4adaeb;
                                padding: 17px;
                                font-size: 23px;
                            }

                            li {
                                list-style: none;
                            }
                            #all_stats span{
                                font-size: large;
                                color: #007898;
                            }
                        </style>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>

<script type="text/javascript">
jQuery.fn.shuffle = function(){
var allElems = this.get();
var getRandom = function(max){
return Math.floor(Math.random() * max);
}
var shuffled = jQuery.map(allElems, function(){
var random = getRandom(allElems.length),
randEl = jQuery(allElems[random]).clone(true)[0];
allElems.splice(random, 1);
return randEl;
});
this.each(function(i){
jQuery(this).replaceWith(jQuery(shuffled[i]));
});
return jQuery(shuffled);
};

// Перемешиваем элементы с классом apo_el, которые лежат внутри блока apo_shuffle
jQuery('#tbody tr').shuffle();

jQuery('#tbody tr').each(function(i){
if(i > 4){
jQuery(this).hide();
}
})
var utc = new Date().toJSON().slice(0,10).replace(/-/g,'/');
jQuery('.time').text(utc);
function randomInteger(min, max) {
  let rand = min + Math.random() * (max - min);
  return Math.round(rand);
}
jQuery('.id').each(function(){
  jQuery(this).text("id"+"-"+randomInteger(1000, 9999) + "-" + randomInteger(100,999))
})
</script>