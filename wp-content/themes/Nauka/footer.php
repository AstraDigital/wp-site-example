<!-- Footer -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9">
                <div class="copyright"><?php echo get_field('текст_в_футере', 'option'); ?></div>
                <div class="footer-logo">
<!--                    <div class="footer-logo-item"><img src="<?php echo get_template_directory_uri(); ?>/img/f-logo-01.png" alt="alt"></div>-->
<!--                    <div class="footer-logo-item"><img src="<?php echo get_template_directory_uri(); ?>/img/f-logo-02.png" alt="alt"></div>-->
                    <div class="footer-logo-item"><img src="<?php echo get_template_directory_uri(); ?>/img/f-logo-03.png" alt="alt"></div>
                    <div class="footer-logo-item"><img src="<?php echo get_template_directory_uri(); ?>/img/f-logo-04.png" alt="alt"></div>

                </div>
            </div>
            <div class="col-lg-3 col-md-3">
                <div class="contacts-block">
                    <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.png" alt="alt"></div><a href="tel:<?php echo get_field('номер_телефона', 'option'); ?>"><?php echo get_field('номер_телефона', 'option'); ?></a>
                </div>
                <div class="contacts-block">
                    <div class="icon"><img src="<?php echo get_template_directory_uri(); ?>/img/email-icon.png" alt="alt"></div><a href=""><span class="__cf_email__"><?php echo get_field('почта', 'option'); ?></span></a>
                </div>
                <div class="terms">
<!--                    <div class="terms-link"><a href="#">Условия пользования</a></div>-->
<!--                    <div class="terms-link"><a href="political.php">Политика уонфиденциальности</a></div>-->
                </div>
            </div>
            <div style="clear:both;"></div>
            <div style="text-align: center; color:#ffffff;"><?php echo get_field('копирайт', 'option'); ?></div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<style>
    .mfp-container{
        position: fixed;
    }
</style>
</body>

</html>