<?php get_header(); ?>  

  
   
    <section id="stats">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8 col-md-offset-2 faq-row" id="all_stats">
                        <?php 
                        the_post();
                        the_title();
                        the_content(); ?>
                        <style>
                            #all_stats li:before {
                                content: "\2666";
                                color: #4adaeb;
                                padding: 17px;
                                font-size: 23px;
                            }

                            li {
                                list-style: none;
                            }
                            #all_stats span{
                                font-size: large;
                                color: #007898;
                            }
                        </style>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
